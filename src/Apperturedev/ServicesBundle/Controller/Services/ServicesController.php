<?php

namespace Apperturedev\ServicesBundle\Controller\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Apperturedev\LoginBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type as Form;

//use Apperturedev\ServicesBundle\Entity\Registrar;

class ServicesController extends Controller
{

    public function indexAction()
    {
        $name = 'Inicio';

        return $this->render('ServicesBundle:Services:main.html.twig', ['titulo' => $name]);
    }

    public function serviciosAction()
    {
        $name = 'Servicios';

        return $this->render('ServicesBundle:Services/dashboard/admin:admin.html.twig', ['titulo' => $name]);
    }

    public function xmlAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('ServicesBundle:Clientes');
        $users = $repository->findAll();

        if (!$users) {
            throw $this->createNotFoundException(
                'No Users'
            );
        }

        return $this->render(
            'ServicesBundle:Services:appersearch.xml.twig',
            ['usuarios' => $users]
        );
    }

    public function UsuariosAction()
    {
        $name = 'Usuarios';
        $repository = $this->getDoctrine()
            ->getRepository('LoginBundle:User');
        $users = $repository->findAll();

        return $this->render(
            'ServicesBundle:Services/dashboard/admin:users.html.twig',
            ['usuarios' => $users, 'titulo' => $name]
        );
    }

    public function addUserAction(Request $request)
    {
        $name = 'Añadir Usuario';
        $nuevousuuario = new User();
        $form = $this->createFormBuilder($nuevousuuario)
            ->add(
                'username',
                Form\TextType::class,
                ['label' => false, 'required' => false, 'attr' => (['placeholder' => 'Usuario'])]
            )
            //->add('password', 'password', array('label' => false, 'required' => false, 'attr' => (array('placeholder' => 'Contraseña'))))
            ->add(
                'password',
                Form\RepeatedType::class,
                [
                    'type' => Form\PasswordType::class,
                    'invalid_message' => 'La contraseña ha de ser iguales',
                    'options' => ['attr' => ['placeholder' => 'Contraseña']],
                    'required' => true,
                    'first_options' => ['label' => false],
                    'second_options' => ['label' => false],
                ]
            )
            ->add('isActive', Form\CheckboxType::class, ['label' => false, 'required' => false])
            ->add('email', Form\EmailType::class, ['label' => false, 'required' => false, 'attr' => (['placeholder' => 'Correo'])])
            ->add(
                'roles',
                Form\ChoiceType::class,
                [
                    'choices' => [
                        'Administrador' => "ROLE_ADMIN",
                        'Super Administrador' => "ROLE_SUPER_ADMIN",
                        'Usuario' => "ROLE_USER",
                    ],
                    'label' => false,
                    'multiple' => true,
                    'required' => false,
                ]
            )
            ->add('registrar', Form\SubmitType::class, ['label' => "Añadir Usuario"])
            ->getForm();
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $password = $this->get('security.password_encoder')
                ->encodePassword($nuevousuuario, $nuevousuuario->getPassword());
            $now = new \DateTime();
            $now->format('Y-m-d');
            $nuevousuuario->setDatecreated($now);
            $nuevousuuario->setPassword($password);
            if ($form->isValid() && $form->isSubmitted()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($nuevousuuario);
                $em->flush();
                $response = true;
            } else {
                $response = false;
            }

            return $this->render(
                'ServicesBundle:Services/dashboard/admin:adduser.html.twig',
                [
                    'titulo' => $name,
                    'form' => $form->createView(),
                    'form_title' => "Añadir Usuario",
                    'respuesta' => $response,
                    'msg' => 'Usuario Añadido',
                ]
            );
        }

        return $this->render(
            'ServicesBundle:Services/dashboard/admin:adduser.html.twig',
            [
                'titulo' => $name,
                'form' => $form->createView(),
                'form_title' => "Añadir Usuario",
                'respuesta' => false,
            ]
        );
    }

    public function editUSerAction($id, request $request)
    {
        $name = 'Editar Usuario';
        $changed = false;
        $repository = $this->getDoctrine()->getManager();
        /** @var User $nuevousuuario */
        $nuevousuuario = $repository->getRepository('LoginBundle:User')->find($id);
        $olddatauser = clone $nuevousuuario;
        if (!$nuevousuuario) {
            throw $this->createNotFoundException(
                'No Users'
            );
        }
        $check = ['username', 'password', 'isActive', 'email', 'roles'];
        $form = $this->createFormBuilder($nuevousuuario)
            ->add(
                'username',
                Form\TextType::class,
                ['label' => false, 'required' => false, 'attr' => (['placeholder' => 'Usuario'])]
            )
            //->add('password', 'password', array('label' => false, 'required' => false, 'attr' => (array('placeholder' => 'Contraseña'))))
            ->add(
                'password',
                Form\RepeatedType::class,
                [
                    'type' => Form\PasswordType::class,
                    'invalid_message' => 'La contraseña ha de ser iguales',
                    'options' => ['attr' => ['placeholder' => 'Contraseña']],
                    'required' => false,
                    'first_options' => ['label' => false],
                    'second_options' => ['label' => false],
                ]
            )
            ->add('isActive', Form\CheckboxType::class, ['label' => false, 'required' => false])
            ->add(
                'email',
                Form\TextType::class,
                ['label' => false, 'required' => false, 'attr' => (['placeholder' => 'Correo'])]
            )
            ->add(
                'roles',
                Form\ChoiceType::class,
                [
                    'choices' => [
                        'Administrador' => "ROLE_ADMIN",
                        'Super Administrador' => "ROLE_SUPER_ADMIN",
                        'Usuario' => "ROLE_USER",
                    ],
                    'label' => false,
                    'multiple' => true,
                    'required' => false
                ]
            )
            ->add('registrar', Form\SubmitType::class, ['label' => "Editar Usuario"])
            ->getForm();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($nuevousuuario->getPassword() == '') {
                    $password = $olddatauser->getPassword();
                    $nuevousuuario->setPassword($password);
                } else {
                    $password = $this->get('security.password_encoder')
                        ->encodePassword($nuevousuuario, $nuevousuuario->getPassword());
                    $nuevousuuario->setPassword($password);
                }
//                foreach ($check as $value) {
//                    if ($olddatauser[$value]!=$nuevousuuario->get($value)){
//                        $changed = true;
//                    }
//                }

                $repository->flush();
                $response = true;

            } else {
                $response = false;
            }

            return $this->render(
                'ServicesBundle:Services/dashboard/admin:adduser.html.twig',
                [
                    'titulo' => $name,
                    'form' => $form->createView(),
                    'form_title' => "Editar Usuario",
                    'respuesta' => $response,
                    'msg' => 'Usuario Editado',
                ]
            );
        }

        return $this->render(
            'ServicesBundle:Services/dashboard/admin:adduser.html.twig',
            [
                'titulo' => $name,
                'form' => $form->createView(),
                'form_title' => "Editar Usuario",
                'respuesta' => false,
            ]
        );
    }


    public function clientesAction()
    {
        $name = 'Clientes';
        $repository = $this->getDoctrine()
            ->getRepository('ServicesBundle:Clientes');
        $clientes = $repository->findAll();

        return $this->render(
            'ServicesBundle:Services/dashboard/admin:clientes.html.twig',
            ['clientes' => $clientes, 'titulo' => $name]
        );
    }


}
