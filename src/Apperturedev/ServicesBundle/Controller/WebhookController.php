<?php


namespace Apperturedev\ServicesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebhookController extends Controller
{


    public function webhookAction(Request $request)
    {
        $jsonData = $request->getContent();

        return new Response($jsonData);
    }

}