<?php

namespace Apperturedev\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Apperturedev\LoginBundle\Entity\User;
//use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Security;

class DefaultController extends Controller
{
     public function loginAction(Request $request)
    {
        $session = $request->getSession();
        
        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                Security::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }
 
        return $this->render(
            'LoginBundle:Login:login.html.twig',
            array(
                'last_username' => $session->get(Security::LAST_USERNAME),
                'error'         => $error,
                'titulo'=>"Login - Apper Search"
            )
        );
    }
}
